package tip064;

import java.util.List;
import java.util.Map;

public class MyModel{
    public Boolean bool;
    public List<String> list;
    public Map<String, String> map;
    public MyUser user;

    public void setBool(Boolean bool){
        this.bool = bool;
    }
    public void setMap(Map<String, String> map){
        this.map = map;
    }
    // マップにはgetterが無いと取得できなかった...
    public Map<String, String> getMap(){
        return this.map;
    }
    public void setList(List<String> list){
        this.list = list;
    }
    public void setUser(MyUser user){
        this.user = user;
    }
    // ユーザリストにはgetterが無いと取得できなかった...
    public MyUser getUser(){
        return this.user;
    }

}