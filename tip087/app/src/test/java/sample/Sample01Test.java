package sample;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;
import org.springframework.boot.test.context.ConfigDataApplicationContextInitializer;

@DisplayName("コンストラクタパターンを動かすテスト")
// @ExtendWith(SpringExtension.class)
// @SpringBootTest
class Sample01Test{
/* 
    private final Sample01 sample01;
    private final String hoge;
    @Autowired
    Sample01Test(Sample01 sample01) {
        this.sample01 = sample01;
        hoge = "文字列";
    }
*/
    @Test
    @DisplayName("何らかのテスト")
    void test01() {
        ApplicationContextRunner contextRunner = new ApplicationContextRunner()
            .withInitializer(new ConfigDataApplicationContextInitializer())
            .withUserConfiguration(TestConfig.class)
            .run((context) -> {
                Sample01 sample01 = context.getBean(Sample01.class);
                assertEquals("文字列", sample01.getText());
             })
        ;
    }
}
